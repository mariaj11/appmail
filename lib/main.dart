import 'package:appmailflutter/model/backend.dart';
import 'package:appmailflutter/model/email.dart';
import 'package:appmailflutter/widgets/email_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(        
        primarySwatch: Colors.pink,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  /*const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
  }*/

  MyHomePage({Key? key}) : super(key: key);

  @override
  _ListScreenState createState()=> _ListScreenState();
}

class _ListScreenState extends State<MyHomePage> {

  @override
    Widget build(BuildContext context) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('App Mail'),
          ),
          body: ListView.builder(
              itemCount: Backend().getEmails().length,
              itemBuilder: (context, index) {
                Email email = Backend().getEmails()[index];
                return Dismissible(
                  key: Key(Backend().getEmails()[index].id.toString()),
                  child: ListTile(
                    subtitle: Text(email.subject),
                    title: Text(email.from),
                    leading: Icon(Icons.brightness_1, color: Colors.pink),
                    //trailing: DateTime(email.dateTime),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ContentWidget(email)));
                    },
                    
                  ),
                  onDismissed: (direction) {
                    Backend().getEmails().remove(index);
                  },
                );
              }));
    }
/*class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: const ListScreen()        
      );
  }*/
}


  

